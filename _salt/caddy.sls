caddy:
  pkgrepo.managed:
    - name: deb https://dl.cloudsmith.io/public/caddy/stable/deb/debian any-version main
    - file: /etc/apt/sources.list.d/caddy.list
    - key_url: https://dl.cloudsmith.io/public/caddy/stable/gpg.key
  pkg.installed: []
  service.running:
    - enable: true
    - reload: true

/var/log/caddy:
  file.directory:
    - require:
      - pkg: caddy
    - user: caddy
    - group: caddy
    - dir_mode: 755

/etc/caddy/Caddyfile:
  file.managed:
    - require:
      - pkg: caddy
    - watch_in:
      - service: caddy
    - contents: |
        {
          email webmaster@doyoueven.click
          # log access {
          #   output file /var/log/caddy/access.jlog
          #   include http.log.access
          # }
        }

        (logging) {
          # For future changes
          log
        }

        {{grains['id']}}.doyoueven.click {
          import logging
          respond "I think therefore I am"
        }

        import sites/*

/etc/caddy/sites:
  file.directory:
    - require:
      - pkg: caddy
    - user: root
    - group: root
    - dir_mode: 755
